package com.javainuse.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "movie")
public class MenuModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    private String title;
    private String author;

    private String image;

    @Column (columnDefinition = "TEXT")
    private String description;

    private String trailler;

    private String release_year;


    private double rating;

    @ManyToOne
    private CategoryModel category;

    public MenuModel() {

    }

    public MenuModel(long id, String title, String author, String image, String description, String trailler, String release_year, double rating, CategoryModel category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.image = image;
        this.description = description;
        this.trailler = trailler;
        this.release_year = release_year;
        this.rating = rating;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTrailler() {
        return trailler;
    }

    public void setTrailler(String trailler) {
        this.trailler = trailler;
    }

    public String getRelease_year() {
        return release_year;
    }

    public void setRelease_year(String release_year) {
        this.release_year = release_year;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @JsonIgnore
    public CategoryModel getCategory() {
        return category;
    }

    @JsonProperty
    public void setCategory(CategoryModel category) {
        this.category = category;
    }
}

