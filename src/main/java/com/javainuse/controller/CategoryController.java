package com.javainuse.controller;

import com.javainuse.model.CategoryModel;
import com.javainuse.model.MenuModel;
import com.javainuse.repository.CategoryRepository;
import com.javainuse.repository.MenuRepository;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api/categorys")
@CrossOrigin(origins = "http://localhost:3000")

public class CategoryController {

    public static final Logger logger = LoggerFactory.getLogger(MenuController.class);

    @Autowired
    private CategoryRepository categoryRepository;


    // -------------------Create a Categorys-------------------------------------------

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createProduct(@RequestBody CategoryModel category) throws SQLException, ClassNotFoundException, IOException {

        logger.info("Creating Category : {}", category);

        categoryRepository.save(category);

        return new ResponseEntity<>(category, HttpStatus.CREATED);
    }


    // -------------------Retrieve All Categorys--------------------------------------------

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<CategoryModel>> listAllProducts() throws SQLException, ClassNotFoundException {

        List<CategoryModel> categorys = categoryRepository.findAll();

        return new ResponseEntity<>(categorys, HttpStatus.OK);
    }


    // -------------------Retrieve Single Category By Id------------------------------------------

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {

        logger.info("Fetching Category with id {}", id);

        CategoryModel category = categoryRepository.findCategoryModelById(id);

        if (category == null) {
            logger.error("Category with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Category with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(category, HttpStatus.OK);
    }


    // ------------------- Update a Category ------------------------------------------------

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody CategoryModel category) throws SQLException, ClassNotFoundException {

        logger.info("Updating Category with id {}", id);

        CategoryModel currentProduct = categoryRepository.findById(id);

        currentProduct.setName(category.getName());

        categoryRepository.save(currentProduct);

        return new ResponseEntity<>(currentProduct, HttpStatus.OK);
    }


    // ------------------- Delete a Category-----------------------------------------

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {

        logger.info("Fetching & Deleting Category with id {}", id);

        categoryRepository.deleteById(id);

        return new ResponseEntity<MenuModel>(HttpStatus.NO_CONTENT);
    }

}
