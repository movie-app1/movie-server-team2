package com.javainuse.controller;

import com.javainuse.model.GameModel;
import com.javainuse.repository.GameRepository;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/game")
@CrossOrigin(origins = "http://localhost:3000")

public class GameController {

    public static final Logger logger = LoggerFactory.getLogger(GameController.class);

    @Autowired
    private GameRepository gameRepository;


    // -------------------Create a Game-------------------------------------------

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createProduct(@RequestBody GameModel game) throws SQLException, ClassNotFoundException, IOException {

        logger.info("Creating Game : {}", game);

        gameRepository.save(game);

        return new ResponseEntity<>(game, HttpStatus.CREATED);
    }


    // -------------------Retrieve All Games--------------------------------------------

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<GameModel>> listAllProducts() throws SQLException, ClassNotFoundException {

        List<GameModel> games = gameRepository.findAll();

        return new ResponseEntity<>(games, HttpStatus.OK);
    }


    // -------------------Retrieve Single Game By Id------------------------------------------

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {

        logger.info("Fetching Game with id {}", id);

        Optional<GameModel> game = Optional.ofNullable(gameRepository.findById(id));

        if (game == null) {
            logger.error("Game with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Game with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(game, HttpStatus.OK);
    }


    // ------------------- Update a Game ------------------------------------------------

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody GameModel game) throws SQLException, ClassNotFoundException {

        logger.info("Updating Game with id {}", id);

        GameModel currentProduct = gameRepository.findById(id);

        currentProduct.setTitle(game.getTitle());
        currentProduct.setImage(game.getImage());
        currentProduct.setDescription(game.getDescription());;

        gameRepository.save(currentProduct);

        return new ResponseEntity<>(currentProduct, HttpStatus.OK);
    }


    // ------------------- Delete a Menu-----------------------------------------

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {

        logger.info("Fetching & Deleting Game with id {}", id);

        gameRepository.deleteById(id);

        return new ResponseEntity<GameModel>(HttpStatus.NO_CONTENT);
    }
}
