package com.javainuse.controller;

import com.javainuse.model.ChannelModel;
import com.javainuse.repository.ChannelRepository;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/channel")
@CrossOrigin(origins = "http://localhost:3000")

public class ChannelController {

    public static final Logger logger = LoggerFactory.getLogger(ChannelController.class);

    @Autowired
    private ChannelRepository channelRepository;


    // -------------------Create a channel-------------------------------------------

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createProduct(@RequestBody ChannelModel channel) throws SQLException, ClassNotFoundException, IOException {

        logger.info("Creating Tv : {}", channel);

        channelRepository.save(channel);

        return new ResponseEntity<>(channel, HttpStatus.CREATED);
    }


    // -------------------Retrieve All channel--------------------------------------------

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<ChannelModel>> listAllProducts() throws SQLException, ClassNotFoundException {

        List<ChannelModel> channels = channelRepository.findAll();

        return new ResponseEntity<>(channels, HttpStatus.OK);
    }


    // -------------------Retrieve Single channel By Id------------------------------------------

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {

        logger.info("Fetching channel with id {}", id);

        Optional<ChannelModel> channel = Optional.ofNullable(channelRepository.findById(id));

        if (channel == null) {
            logger.error("channel with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Game with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(channel, HttpStatus.OK);
    }


    // ------------------- Update a chanel ------------------------------------------------

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody ChannelModel channel) throws SQLException, ClassNotFoundException {

        logger.info("Updating channel with id {}", id);

        ChannelModel currentProduct = channelRepository.findById(id);

        currentProduct.setTitle(channel.getTitle());
        currentProduct.setImage(channel.getImage());

        channelRepository.save(currentProduct);

        return new ResponseEntity<>(currentProduct, HttpStatus.OK);
    }


    // ------------------- Delete a Menu-----------------------------------------

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {

        logger.info("Fetching & Deleting channel with id {}", id);

        channelRepository.deleteById(id);

        return new ResponseEntity<ChannelModel>(HttpStatus.NO_CONTENT);
    }
}
