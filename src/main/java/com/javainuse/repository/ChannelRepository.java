package com.javainuse.repository;

import com.javainuse.model.ChannelModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChannelRepository extends JpaRepository<ChannelModel, Long> {
    ChannelModel findById(long id);
}
