package com.javainuse.repository;

import com.javainuse.model.MenuModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuRepository extends JpaRepository<MenuModel, Long> {
    MenuModel findById(long id);
}
