package com.javainuse.repository;


import com.javainuse.model.DAOUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<DAOUser, Integer> {
    DAOUser findByUsername(String username);

    DAOUser findById(long userId);
}