package com.javainuse.repository;

import com.javainuse.model.GameModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRepository extends JpaRepository<GameModel, Long> {
    GameModel findById(long id);
}
